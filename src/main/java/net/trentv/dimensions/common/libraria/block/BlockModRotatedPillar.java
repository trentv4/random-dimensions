package net.trentv.dimensions.common.libraria.block;

import net.minecraft.block.BlockRotatedPillar;
import net.minecraft.block.material.Material;

public class BlockModRotatedPillar extends BlockRotatedPillar
{
	// BlockRotatedPillar's constructor is protected - this just exposes it.
	public BlockModRotatedPillar(Material materialIn)
	{
		super(materialIn);
	}
}
